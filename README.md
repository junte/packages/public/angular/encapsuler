# Junte Encapsuler

## Version

[npm v0.0.38](https://www.npmjs.com/package/junte-encapsuler)

## Installation

* Install Junte Encapsuler with `npm install junte-encapsuler --save`

* Once installed you need add some scripts to `package.json` for encapsulate your components

>"encapsulation": "gulp build --gulpfile node_modules/junte-encapsuler/encapsulation.js",  
"encapsulation:watch": "gulp build --gulpfile node_modules/junte-encapsuler/encapsulation.js --watch"  

* Use npm run encapsulation (:watch) for encapsulate your components templates and styles.

## Encapsulation

### [host] and [_host] attributes:

In order for our encapsulation to work, you need to adds the host='selector' attribute using @HostBinding, this will provide an encapsulation of component styles.  

To encapsulate the styles of elements inside a component, our encapsulator automatically adds the '_selector' attribute to them.  

## Examples

Bind host attribute to `test.component.ts` file  

```js
@HostBinding('attr.host') host = 'app-test-host';
```

Add `build.json` to component folder:

```json
{
    "encapsuler": [
    {
      "host": "app-test-host",
      "html": {
        "from": "test.component.html",
        "to": "test.encapsulated.html"
      },
      "scss": {
        "from": "test.component.scss",
        "to": "../../assets/styles/components/test/test.scss"
      }
    }
  ]
}
```
        
Change templateUrl and remove styleUrls to test.encapsulated

```js
templateUrl: './test.encapsulated.html'
```

Import encapsulated style from builder path to global loaded styles

```scss
@import "assets/styles/components/test/test";
```

### [host] and [child-of] example:

`test.component.html` before
```html 
<h1>Test component</h1>
<form>
    <input type="text">
    <app-button>Test button</app-button>
</form>
```  

`test.encapsulated.html` after:
```html 
<h1 _at>Test component</h1>
<form child-of="app-test-host">
    <input child-of="app-test-host" type="text"/>
    <app-button child-of="app-test-host">Test button</button>
</form>
```  

`test.component.scss` before:
```scss 
@import "variables";
@import "../button/button";

:host {
    display: block;
    padding: 16px;
    border: 1px solid $jnt-primary-color;
}

h1 {
    color: $jnt-primary-color;
}

form {
    background-color: $jnt-secondary-background;
    padding: 8px;

    input {
    margin-right: 16px;
    border-radius: $jnt-corner-normal;
    padding: 8px 16px;
  }

  @include app-button((
    padding: $jnt-gutter-normal,
    color: $jnt-success-color
  ), '&', false, 'merge');
}
```  

`test.scss` after:
```scss 
@import "variables";
@import "../button/button";

[host=app-test-host] {
    display: block;
    padding: 16px;
    border: 1px solid $jnt-primary-color;
}

h1[child-of=app-test-host] {
    color: $jnt-primary-color;
}

form[child-of=app-test-host] {
    background-color: $jnt-secondary-background;
    padding: 8px;

    input[child-of=app-test-host] {
    margin-right: 16px;
    border-radius: $jnt-corner-normal;
    padding: 8px 16px;
  }

  @include app-button((
    padding: $jnt-gutter-normal,
    color: $jnt-success-color
  ), '&', false, 'merge');
}
```  

![picture alt](./src/assets/images/test.png)
