import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { JunteUiModule } from 'junte-ui';
import { ButtonModule } from '../button/button.module';
import { TestComponent } from './test.component';

@NgModule({
    declarations: [TestComponent],
    imports: [
        CommonModule,
        JunteUiModule,
        ButtonModule
    ],
    exports: [TestComponent]
})
export class TestModule {
}
