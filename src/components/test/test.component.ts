import { Component, HostBinding } from '@angular/core';
import { UI } from 'junte-ui';

@Component({
    selector: 'app-test',
    templateUrl: './test.encapsulated.html'
})
export class TestComponent {

    ui = UI;

    @HostBinding('attr.host') host = 'app-test-host';
}
