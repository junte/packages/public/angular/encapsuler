import { Component, HostBinding } from '@angular/core';

@Component({
    selector: 'app-button',
    templateUrl: './button.encapsulated.html'
})
export class ButtonComponent {

    @HostBinding('attr.host') host = 'app-button-host';

}
