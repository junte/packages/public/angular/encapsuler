import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { PrismModule } from '@ngx-prism/core';
import { JunteUiModule } from 'junte-ui';
import { TestModule } from 'src/components/test/test.module';
import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot([]),
        BrowserAnimationsModule,
        JunteUiModule,
        PrismModule,
        TestModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
