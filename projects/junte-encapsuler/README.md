# Junte Encapsuler

## Version

[npm v0.0.26](https://www.npmjs.com/package/junte-encapsuler)

##Installation

* Install Junte Encapsuler with `npm install junte-encapsuler --save`

* Once installed you need add some scripts to `package.json` for encapsulate your components

>"encapsuler": "mkdir -p src/scripts && cp ./node_modules/junte-encapsuler/encapsulation.ts./src/scripts",  
"encapsulation": "gulp build --gulpfile ./src/scripts/encapsulation.ts",  
"encapsulation:watch": "gulp build --gulpfile ./src/scripts/encapsulation.ts --watch"  

* Use npm run encapsuler for copy gulpfile from `node_modules` to `your_project/scripts/encapsulation.ts`

* Use npm run encapsulation (:watch) for encapsulate your components templates and styles.

##Encapsulation

###[host] and [_host] attributes:

In order for our encapsulation to work, you need to adds the host='selector' attribute using @HostBinding, this will provide an encapsulation of component styles.  

To encapsulate the styles of elements inside a component, our encapsulator automatically adds the '_selector' attribute to them.  

###[child-host] attribute:

To override the styles of the encapsulated component, use the [child-host=#{'$child-selector'}] attribute in the parent's scss file

##Examples

Bind host attribute to `test.component.ts` file  

```js
@HostBinding('attr.host') host = 'app-test-host';
```

Add this host to any loaded scss style (`variables.scss` for example)  

```scss
$app-test-host: 'app-test-host';
```

Change templateUrl and remove styleUrls to test.encapsulated

```js
templateUrl: './test.encapsulated.html'
```

Import encapsulated style to global loaded styles

```scss
@import "components/test/test.encapsulated";
```

###[host] and [child-of] example:

`test.component.html` before
```html 
<h1>Test component</h1>
<form>
    <input type="text">
    <button>Test button</button>
</form>
```  

`test.encapsulated.html` after:
```html 
<h1 child-of="app-test-host">Test component</h1>
<form child-of="app-test-host">
    <input child-of="app-test-host" type="text"/>
    <button child-of="app-test-host">Test button</button>
</form>
```  

`test.component.scss` before:
```scss 
@import "variables";

:host {
    display: block;
    padding: 16px;
    border: 1px solid $primary-color;
}

h1 {
    color: $primary-color;
}

form {
    background-color: $secondary-background;
    padding: 8px;

    input {
    margin-right: 16px;
    border-radius: $corner-normal;
    padding: 8px 16px;
  }

  button {
    background-color: $primary-color;
    color: white;
    border-radius: $corner-normal;
    padding: 8px 16px;
  }
}
```  

`test.encapsulated.scss` after:
```scss 
@import "variables";

app-test[host=#{$app-test-host}] {
    display: block;
    padding: 16px;
    border: 1px solid $primary-color;
}

h1[child-of=#{$app-test-host}] {
    color: $primary-color;
}

form[child-of=#{$app-test-host}] {
    background-color: $secondary-background;
    padding: 8px;

    input[child-of=#{$app-test-host}] {
    margin-right: 16px;
    border-radius: $corner-normal;
    padding: 8px 16px;
  }

  button[child-of=#{$app-test-host}] {
    background-color: $primary-color;
    color: white;
    border-radius: $corner-normal;
    padding: 8px 16px;
  }
}
```  

###[child-host] example:

`test-child.component.scss` before:
```scss 
@import "variables";

:host {
    display: block;
    background-color: #4F4F4F;
    padding: 16px;
    margin: 8px 0;
    color: white;
}
``` 

`test-child.encapsulated.scss` after:
```scss 
@import "variables";

app-test-child[host=#{$app-test-child-host}] {
    display: block;
    background-color: #4F4F4F;
    padding: 16px;
    margin: 8px 0;
    color: white;
}
``` 

`test-parent.component.scss` before:
```scss 
@import "variables";

:host {
    display: block;
    background-color: #E9E9E9;
    padding: 16px;
    margin: 8px 0;
}

app-test-child[child-host=#{$app-test-child-host}] {
    background-color: white;
    color: #4F4F4F;
}
``` 

`test-parent.encapsulated.scss` after:
```scss 
@import "variables";

app-test-parent[host=#{$app-test-parent-host}] {
    display: block;
    background-color: #E9E9E9;
    padding: 16px;
    margin: 8px 0;
}

app-test-child[host=#{$app-test-child-host}][child-of=#{$app-test-parent-host}] {
    background-color: white;
    color: #4F4F4F;
}
``` 
